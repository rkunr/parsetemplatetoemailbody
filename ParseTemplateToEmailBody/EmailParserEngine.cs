﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RazorEngine.Templating;
using RazorEngine.Configuration;
using System.IO;


namespace ParseTemplateToEmailBody
{
    public class EmailParserEngine
    {
        private readonly string _fromName;
        private readonly string _fromEmail;
        private readonly IRazorEngineService _service;



        /// <summary>
        /// Constructs a EmailParserEngine instance
        /// </summary>
        /// <param name="templatePath">Path of the template to load(example, @"folderName\templateName"). The template's Build Action should be set to Content and the Copy to Output Directory flag set to Copy Always</param>
        public EmailParserEngine(string templatePath) : this(templatePath, null, null)
        {
        }

        /// <summary>
        /// EmailParserEngine instance which is responsible for converting Razor templates into string i.e., mail body and any mail program can be used to send email
        /// </summary>
        /// <param name="templatePath">Path of the template to load(example, @"folderName\templateName"). The template's Build Action should be set to Content and the Copy to Output Directory flag set to Copy Always</param>
        /// <param name="fromName">Sender's Name example, person name or company name</param>
        /// <param name="fromEmail">Sender's email address, i.e., email address from which this email is being sent</param>
        public EmailParserEngine(string templatePath, string fromName, string fromEmail)
        {
            _fromName = fromName;
            _fromEmail = fromEmail;
            var templatePathForWebApp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin", templatePath);

            var templatePathForConsoleApp = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, templatePath);

            var config = new TemplateServiceConfiguration
            {
                TemplateManager = new ResolvePathTemplateManager(new[] { templatePathForWebApp, templatePathForConsoleApp })
            };
            _service = RazorEngineService.Create(config);
        }

        /// <summary>
        /// Creates email body as a string from given template and model
        /// </summary>        
        /// <param name="templateName">The name of the Razor template(f.eks, AgendaEmailTemplate.cshtml) without EXTENSION. Write only (AgendaEmailTemplate)</param>
        /// <param name="model">A model containing data to populate/assign in the Razor template</param>
        /// <returns></returns>
        public virtual string CreateEmail<T>(string templateName, T model)
        {
            var key = _service.GetKey(templateName);
            return _service.RunCompile(key, typeof(T), model);
        }


        /// <summary>
        /// When everything is done, dispose the underlying RazorEngine service
        /// </summary>
        public void DisposeRazorEngineService()
        {
            _service.Dispose();
        }

    }
}
